Test Synolia

1- récupération du projet 
    -> git clone https://gitlab.com/badreddine.boukhalfa/synolia.git

2- acceder au projet via le cmd:
    -> cd synolia

3- installer les dépendances via composer 
    -> composer install 

4- creation de la base donnée:
    -> php bin/console doctrine:database:create

5- creation tables : entity IP,User,contact 
    ->  php bin/console doctrine:schema:update --force
    

6- mettre des données aléatoire dans la base données 
    -> php bin/console doctrine:fixtures:load
    ->  User admin : 
        -> mail: admin@gmail.com 
        -> mot de passe : password

7-  lancer le serveur 
    -> php -S 127.0.0.1:8000 -t public

8- accéder à la page d'acceuil 

    ->http://127.0.0.1:8000/

9- accéder à la page entity IP
    ->http://127.0.0.1:8000/admin/list/fr ('n'oublie de se connecter en tant que admin :) )


Merci



