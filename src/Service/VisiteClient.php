<?php

namespace App\Service;

use App\Entity\ClientIp;
use App\Repository\ClientIpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;



class VisiteClient
{
    
    private $em;

    private $ipClient;

    private $session ;

    function __construct(EntityManagerInterface $em,ClientIpRepository $ipClient,SessionInterface $session)
    {
        $this->em=$em;
        $this->ipClient=$ipClient;
        $this->session=$session;

    }
    public function saveAddressIp($request)
    {
        $this->session->start();

        $visite=$this->session->get('visite');
        if(!$visite)
        {
            $this->session->set('visite',true);

            $ipAdress=$request->getClientIp();
            $data= $this->ipClient->findBy(['ipAddress' => $ipAdress]);
            if ($data)
            {
                $date = new \DateTime();
                $data[0]->setNombreVisite();
                $data[0]->preUpdate($date);
                $this->em->persist($data[0]);
                $this->em->flush();
            }else{ 
    
                $clientIp=new ClientIp();
                $clientIp->setIpAddress($ipAdress);
                $this->em->persist($clientIp);
                $this->em->flush();
            }

        }
       
        
    }
}