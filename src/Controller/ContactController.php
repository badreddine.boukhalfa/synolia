<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Event\ContactEmailEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact/{_locale}", name="contact",methods={"GET","POST"},requirements={
     *         "_locale": "en|fr",
     *     }))
     */
    public function index(Request $request,EventDispatcherInterface $dispatcher,TranslatorInterface $translator): Response
    {
        $contact=new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            
            $ContactEmailEvent = new ContactEmailEvent($form->getData());
            $dispatcher->dispatch($ContactEmailEvent, 'contact.success');
          
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $this->addFlash(
                'info',
                $translator->trans('votre mail a été envoyé avec succée !')
            );
            return $this->redirectToRoute('contact');
        }
        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
