<?php

namespace App\Controller;

use App\Service\VisiteClient;
use App\Repository\ClientIpRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;


class HomeController extends AbstractController
{

    private $ipClient;

    function __construct(ClientIpRepository $ipClient)
    {
        $this->ipClient=$ipClient;
    

    }

    /**
     * @Route("/home/{_locale}", name="home",requirements={
     *         "_locale": "en|fr",
     *     })
     */
    public function index(VisiteClient $addAdressIp,Request $request ): Response
    {

        $addAdressIp->saveAddressIp($request);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("admin/list/{_locale}", name="list",requirements={
     *         "_locale": "en|fr",
     *     })
     */
    public function listVisite(PaginatorInterface $paginator,Request $request ,CacheInterface $cache): Response
    {
        $data=$cache->get('visite',function(ItemInterface $item){
            $item->expiresAfter(1800);
            return $this->ipClient->findAllClient();

        });
        
       $visite = $paginator->paginate(
        $data, 
        $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
        10
    );

        return $this->render('home/list.html.twig', [
            'visites' => $visite,
        ]);
    }

    /**
     * @Route("/list/{id}", name="detail_visite")
     */
    public function detailVisite($id,Request $request): Response
    {
        $visite= $this->ipClient->find($id);

        return $this->render('home/detail.html.twig', [
            'visite' => $visite,
        ]);
    }

    

}
