<?php

namespace App\EventDispatcher;

use App\Entity\User;
use App\Event\ContactEmailEvent;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Security;

class ContactSubscriber implements EventSubscriberInterface
{
    protected $logger;
    protected $mailer;

    public function __construct(LoggerInterface $logger, MailerInterface $mailer)
    {
        $this->logger = $logger;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            'contact.success' => 'sendContactEmail'
        ];
    }

    public function sendContactEmail(ContactEmailEvent $contactEmailEvent)
    {

        // 1. Récupérer le message (je la trouverai dans PurchaseSuccessEvent)
        $message = $contactEmailEvent->getMessage();

        // 2. Ecrire le mail (nouveau TemplatedEmail)
        $email = new TemplatedEmail();
        $email->to(new Address($message->getMail(),'customersContact'))
            ->from("tsynolia@gmail.com")
            ->subject("réclamation, un message de la part de : ({$message->getNom()} - {$message->getPrenom()} )")
            ->htmlTemplate('emails/contact.html.twig')
            ->context([
                'message' => $message->getMessage(),
            ]);

        // 3. Envoyer l'email 
        $this->mailer->send($email);

    }

    
}
