<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Contact;

class ContactEmailEvent extends Event
{
    protected $message;

    public function __construct(Contact $form)
    {
        $this->message = $form;
    }

    public function getMessage(): Contact
    {
        return $this->message;
    }
}
