<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\ClientIp;
use Faker\Provider\Internet;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

   
    protected $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
 
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
      //  $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new \Faker\Provider\Internet($faker));

        for ($u = 0; $u < 20; $u++) {
            $client = new ClientIp();

            $client->setIpAddress($faker->ipv4());
            $client->preUpdate($faker->dateTimeBetween('-6 months'));

            for ($i=0;$i<mt_rand(1, 20);$i++)
            {
                $client->setNombreVisite();
            }
                
            $manager->persist($client);

        }

        $admin = new User;

        $hash = $this->encoder->encodePassword($admin, "password");

        $admin->setMail("admin@gmail.com")
            ->setPassword($hash)
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        //$users = [];

        for ($u = 0; $u < 5; $u++) {
            $user = new User();

            $hash = $this->encoder->encodePassword($user, "password");

            $user->setMail("user$u@gmail.com")
                ->setPassword($hash)
                ->setRoles(['ROLE_USER']);

            //$users[] = $user;

            $manager->persist($user);
        }

        $manager->flush();

    }
}
