<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ClientIpRepository;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClientIpRepository::class)
 */
class ClientIp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Ip
     */
    private $ipAddress;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $dateVisite;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombreVisite;

    public function __construct()
    {
        $this->dateVisite= new \DateTime();
        $this->nombreVisite=1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getDateVisite()
    {
        return $this->dateVisite;
    }

     /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(DateTime $date)
    {
        $this->dateVisite= $date;
    }

    public function getNombreVisite(): ?int
    {
        return $this->nombreVisite;
    }

    
    public function setNombreVisite(): self
    {
        $this->nombreVisite = $this->nombreVisite + 1;

        return $this;
    }

    


}
