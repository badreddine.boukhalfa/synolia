<?php

namespace App\Repository;

use App\Entity\ClientIp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientIp|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientIp|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientIp[]    findAll()
 * @method ClientIp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientIpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientIp::class);
    }

    // /**
    //  * @return ClientIp[] Returns an array of ClientIp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientIp
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllClient()
    {
        return $this->findBy(array(), array('nombreVisite' => 'DESC'));
    }
}
